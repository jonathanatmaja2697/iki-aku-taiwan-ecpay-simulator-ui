// eslint-disable-next-line react-hooks/exhaustive-deps
import { TextField } from "@mui/material";
import React from "react";

const Input = ({value, handleChange}) => {
  return (
    <TextField
      id="payment-no"
      label="Payment No"
      variant="outlined"
      value={value}
      onChange={handleChange}
    />
  );
};

export default Input;
