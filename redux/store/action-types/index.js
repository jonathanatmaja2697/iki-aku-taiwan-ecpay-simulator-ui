const SET_TRANSACTION_LIST = "transaction-list";
const SET_LOADING = "set-loading";
const SET_DISMISS_LOADING = "set-dismiss-loading";

export { SET_TRANSACTION_LIST, SET_LOADING, SET_DISMISS_LOADING };
