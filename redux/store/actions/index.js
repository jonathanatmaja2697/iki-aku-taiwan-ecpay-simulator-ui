import axios from "axios";
import {
  SET_TRANSACTION_LIST,
  SET_LOADING,
  SET_DISMISS_LOADING,
} from "../action-types";

const url = "https://ecpay.develoops.my.id";

const setTransactionList = (payload) => ({
  type: SET_TRANSACTION_LIST,
  payload,
});

const showLoading = (payload) => ({
  type: SET_LOADING,
  payload,
});

const dismissLoading = (payload) => ({
  type: SET_DISMISS_LOADING,
  payload,
});

const getLatestTransaction = () => (dispatch) => {
  return new Promise((resolve, reject) => {
    console.log(url + "/transaction/latest");
    axios
      .get(`${url}/transaction/latest`)
      .then((res) => {
        console.log(res.data);
        dispatch(setTransactionList(res.data.data));
        resolve(res.data.data);
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
};

const executeCallback =
  (request, showLoading, dismissLoading) => (dispatch) => {
    return new Promise((resolve, reject) => {
      dispatch(showLoading());
      axios
        .post(`${url}/callback`, request)
        .then((res) => {
          console.log(res.data);
          resolve(res.data);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        })
        .finally(() => {
          dispatch(dismissLoading());
        });
    });
  };

const payTransaction =
  (paymentNo, showLoading, dismissLoading) => (dispatch) => {
    return new Promise((resolve, reject) => {
      dispatch(showLoading());
      axios
        .post(`${url}/pay`, { payment_no: paymentNo })
        .then((res) => {
          alert(res.data.message);
          resolve(res.data);
        })
        .catch((err) => {
          alert(err.response.data.message);
          reject(err);
        })
        .finally(() => {
          dispatch(dismissLoading());
        });
    });
  };

export {
  getLatestTransaction,
  showLoading,
  dismissLoading,
  executeCallback,
  payTransaction,
};
