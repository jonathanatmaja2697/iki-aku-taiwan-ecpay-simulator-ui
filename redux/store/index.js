import thunk from "redux-thunk";
import { combineReducers, createStore, applyMiddleware } from "redux";
import { mainReducer } from "./reducers";

const appReducers = combineReducers({
  main: mainReducer,
});

const rootReducer = (state, action) => {
  return appReducers(state, action);
};

const configureReducer = () => {
  return createStore(rootReducer, applyMiddleware(thunk));
};

export default configureReducer;
