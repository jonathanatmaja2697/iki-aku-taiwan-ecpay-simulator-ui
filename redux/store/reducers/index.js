import {
  SET_DISMISS_LOADING,
  SET_LOADING,
  SET_TRANSACTION_LIST,
} from "../action-types";

const initState = {
  transactionList: [],
  loading: false,
};

const mainReducer = (state = initState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SET_TRANSACTION_LIST:
      return { ...state, transactionList: payload };
    case SET_LOADING:
      return { ...state, loading: true };
    case SET_DISMISS_LOADING:
      return { ...state, loading: false };
    default:
      return state;
  }
};

export { mainReducer };
