import React from "react";
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import Head from "next/head";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  dismissLoading,
  executeCallback,
  getLatestTransaction,
  payTransaction,
  showLoading,
} from "../redux/store/actions";
import styles from "../styles/Home.module.css";
import { Input } from "../components";

const Index = () => {
  const dispatch = useDispatch();
  const { transactionList, loading } = useSelector((state) => state.main);
  const [open, setOpen] = useState(false);
  const [tradeNo, setTradeNo] = useState(null);
  const [paymentNo, setPaymentNo] = useState("");
  const runCallback = (tradeNo) => {
    const request = {
      trade_no: tradeNo,
    };
    dispatch(executeCallback(request, showLoading, dismissLoading)).then(() => {
      location.reload();
      onClose();
    });
  };
  const onOpen = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    dispatch(getLatestTransaction());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = (e) => {
    setPaymentNo(e.target.value);
  };

  const pay = () => {
    dispatch(payTransaction(paymentNo, showLoading, dismissLoading));
  };

  const ConfirmDialog = ({ open, onClose, tradeNo }) => {
    return (
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>Konfirmasi callback</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Proses <b>{tradeNo}</b> ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose}>Batal</Button>
          <Button
            onClick={() => {
              runCallback(tradeNo);
            }}
            disabled={loading}
          >
            {loading ? <CircularProgress /> : "Ok"}
          </Button>
        </DialogActions>
      </Dialog>
    );
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>IKI AKU Taiwan Dev Ecpay Simulator</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>IKI AKU Taiwan Dev</h1>
        <div
          style={{
            alignSelf: "flex-start",
            marginBottom: "2%",
            marginTop: "5%",
            flexDirection: "row",
          }}
        >
          <Input
            value={paymentNo}
            handleChange={(e) => setPaymentNo(e.target.value)}
          />
          <span> </span>
          <Button
            variant="outlined"
            sx={{ height: 56 }}
            onClick={() => pay()}
            disabled={loading}
          >
            Bayar
          </Button>
        </div>
        {transactionList.length > 0 ? (
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }}>
              <TableHead>
                <TableRow>
                  <TableCell>No</TableCell>
                  <TableCell>Trade No</TableCell>
                  <TableCell>Date</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell>Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {transactionList.map((x, i) => (
                  <TableRow key={i}>
                    <TableCell component="th" scope="row">
                      {i + 1}
                    </TableCell>
                    <TableCell>{x.tradeNo}</TableCell>
                    <TableCell>{x.date}</TableCell>
                    <TableCell>{x.amount}</TableCell>
                    <TableCell>
                      <Button
                        variant="outlined"
                        onClick={() => {
                          onOpen();
                          setTradeNo(x.tradeNo);
                        }}
                      >
                        Process
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        ) : (
          <h3>No data to process!</h3>
        )}
        <ConfirmDialog onClose={onClose} open={open} tradeNo={tradeNo} />
      </main>
    </div>
  );
};

export default Index;
